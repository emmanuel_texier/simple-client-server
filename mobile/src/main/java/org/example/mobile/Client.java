package org.example.mobile;

import java.util.concurrent.Future;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.util.EntityUtils;

/**
 * A simple asynchronous but blocking http client
 */
public class Client {
  public static void main(String[] args) throws Exception {
    try (CloseableHttpAsyncClient httpClient = HttpAsyncClients.createDefault()) {
      HttpGet httpget = new HttpGet("http://localhost:8080/");
      System.out.println("Executing request " + httpget.getRequestLine());
      httpClient.start();
      Future<HttpResponse> future = httpClient.execute(httpget, null, null);
      HttpResponse response = future.get();
      System.out.println("Response status: " + response.getStatusLine());
      System.out.println("Response body: " + EntityUtils.toString(response.getEntity()));
    }
  }
}
