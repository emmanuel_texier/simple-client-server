package org.example.backend;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;

import java.io.IOException;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;

/**
 * A simple hello world server
 */
public class HelloWorld extends AbstractHandler {
  public void handle(String target,
                     Request baseRequest,
                     HttpServletRequest request,
                     HttpServletResponse response)
      throws IOException, ServletException {
    response.setContentType("text/plain;charset=utf-8");
    response.setStatus(HttpServletResponse.SC_OK);
    baseRequest.setHandled(true);
    String method = request.getMethod();
    if (method.equals("GET")) {
      response.getWriter().println("Hello World");
    } else {
      response.getWriter().print("Unsupported method");
      response.setStatus(500);
    }
  }

  /**
   * Main method starting server and waiting for requests
   * @param args unused
   * @throws Exception if an error occurs
   */
  public static void main(String[] args) throws Exception {
    Server server = new Server(8080);
    server.setHandler(new HelloWorld());
    server.start();
    server.join();
  }
}