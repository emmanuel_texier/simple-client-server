Introduction
------------
 This exercise is open-ended, and is designed to give you an opportunity to not only show your coding and O-O skills but
 also your creativity, analytical and design skills.

 We expect you to return the exercise within 2h. However, you are free to spend more time later on to
 refine/correct/complete your initial submission.

 The exercise includes a jumpstart client-server maven project simulating a mobile device/backend interactions.
 It uses the popular lightweight Jetty server and HttpComponent Apache client.
 It is given to you as a possible starting point, but you are free to use any other technology or language you want:
 (if so, be sure to provide instructions to compile and run it).

Subject
-------
Design and implement an Client API that reliably invokes a Helloworld service.
The API should persist and retry undelivered requests in case the server is not responding or
the client is in offline mode.

How to start the server
-----------------------
 > cd <root>/backend
 > mvn clean compile exec:java
 // Server will be listening on 8080. You can try http://localhost:8080

How to run client
------------------
 > cd <root>/mobile
 > mvn clean compile exec:java -q
 // check you get Hello World


Details
--------

  In a real mobile scenario, mobile connectivity is unreliable, wireless connection may be flaky, phone may be turned
  off or put in airplane mode.

  the mobile developer should be able to configure the mobile API so it automatically cache and retries undelivered
  requests until a certain time expiration or max retries are exceeded.

 - Expose a client-side reliable messaging API to customize caching and retries upon network or server unavailability.
 - Design a client-side simple connection status component that the http client can check before invoking the service
 - Implement and test the API.
 - (BONUS Feature 1): API contract should be honored even if the client/device is restarted.
 - (BONUS Feature 2): ensure that server processes requests only once.

 Do not hesitate to comment potential pitfalls or un-handled corner cases in your code. It does not have to be production
 ready, make it work right first, then optimize if you have time.